#!/usr/bin/env node
import { resolve } from "path";
import { readFile, writeFile } from "fs/promises";

const QUERY = `repo:"mediawiki/core" AND path:"^resources/(src|lib)/(vue|pinia)/.*"`;
const TS_FILE = resolve(
    process.env.TOOL_DATA_DIR,
    "gerrit-vue-watcher/timestamp.txt",
);
const WEBHOOK_URL = process.env.GERRIT_WEBHOOK_URL;

const lastRun = (await readFile(TS_FILE, "utf-8")).trim();
const now = new Date();

const res = await fetch(
    `https://gerrit.wikimedia.org/r/changes/?q=${encodeURIComponent(`${QUERY} AND after:"${lastRun}"`)}&n=25`,
    {
        headers: {
            "Accept": "application/json",
            "User-Agent":
                "gerrit-vue-watcher-bot/0.1 (https://gitlab.wikimedia.org/sportz/gerrit-vue-watcher; wiki@sportshead.dev)",
        },
    },
)
    .then((r) => r.text())
    .then((r) => JSON.parse(r.slice(5))); // remove ")]}'\n"

const embeds = [];
for (const change of res) {
    const task = change.topic.replaceAll(/T\d+/g, "[$&](https://phabricator.wikimedia.org/$&)");
    const embed = {
        title: change.subject,
        url: `https://gerrit.wikimedia.org/r/c/mediawiki/core/+/${change._number}`,
        timestamp: change.updated.replace(" ", "T"),
        fields: [
            {
                name: "Status",
                value: change.status,
                inline: true,
            },
            {
                name: "Project",
                value: `[${change.project}](https://gerrit.wikimedia.org/g/${change.project})`,
                inline: true,
            },
            {
                name: "Topic",
                value: task,
            },
        ],
    };
    embeds.push(embed);
}

while (embeds.length) {
    const send = embeds.splice(0, 5);
    const res = await fetch(WEBHOOK_URL, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({ embeds: send }),
    });
    const json = await res.text();
    if (res.status !== 204) {
        console.error(json);
        throw `discord error ${res.status} ${res.statusText}`;
    }
}

const pad = (n) => (n < 10 ? `0${n}` : n);
await writeFile(
    TS_FILE,
    `${now.getUTCFullYear()}-${pad(now.getUTCMonth() + 1)}-${pad(now.getUTCDate())} ${pad(now.getUTCHours())}:${pad(now.getUTCMinutes() - 1)}:00`,
);
