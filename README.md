# gerrit-vue-watcher
Small cronjob running on [Toolforge](https://wikitech.wikimedia.org/wiki/Portal:Toolforge) as the [ultraviolet](https://toolsadmin.wikimedia.org/tools/id/ultraviolet) tool.

Uses the gerrit API to look for changes matching a query (currently `repo:"mediawiki/core" AND path:"^resources/(src|lib)/(vue|pinia)/.*"`, see [check.mjs](./check.mjs)).
Any changes updated since the last run will be sent to a Discord webhook.

